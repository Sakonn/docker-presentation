## Čo to Docker je?
![Docker2](container-vm-whatcontainer.png)
![Docker](container-what-is-container.png)

## Trošku z histórie tejto technológie

Docker bol publikovaný v roku 2013 ako open-source projekt a aktuálne je to najrozšírenejšia platforma svojho druhu. Pôvodne Docker fungoval na Linuxe, ale neskôr si aj Microsoft uvedomil výhody tohto operačného systému a v roku 2016 bola vydaná verzia, ktorá podporovala Win server.

Dodnes je ale podpora na Windowse obmedzená iba na Windows 10 v Pro, Enterprise alebo Education verzii kvôli požadovaniu Hyper-V. Tieto špecifické požiadavky pre Windows sú z dôvodu rozdielnosti operačného systému oproti Linuxu.

## Inštalácia
https://docs.docker.com/engine/install/debian/
```
curl -fsSL https://get.docker.com -o get-docker.sh
sudo sh get-docker.sh
```

## Container vs. Image

Docker umožňuje vytvoriť vlastný kontajner, ktorý bude obsahovať zložky potrebné pre konkrétny projekt. Mnohokrát je ale jednoduchšie nájsť si konkrétny kontajner, ktorý spĺňa požiadavky (mysql, nginx, php). Tu ale je rozdiel v tom, čo je container a čo je image.

- **Image** - je súbor v, ktorom je definované ako má container vyzerať. Čo má obsahovať a pod.
- **Container** - je už konkrétna inštancia, ktorá beží na hosťovskom pc.


## Vytvorenie docker image
Python based on: debian
Alternatíva ja napr apline image (cca 5MB)

Zoznam images: https://hub.docker.com/search?q=&type=image

## Python v containeri
V súbore Dockerfile je popísaný celý container.
Zdrojové containery:
- https://github.com/docker-library/python/blob/master/3.8/buster/slim/Dockerfile
- https://github.com/debuerreotype/docker-debian-artifacts/blob/0590e3720f7f87992202bfcca4c13c374ff304bc/bullseye/Dockerfile
- Resp manifest pre debian: https://github.com/debuerreotype/docker-debian-artifacts/blob/0590e3720f7f87992202bfcca4c13c374ff304bc/buster/slim/rootfs.manifest


## Commands
```bash
docker build --tag python-docker .

docker run python-docker
docker run python-docker --name rest-server
docker run python-docker --name rest-server -p 5000:5000
docker run -p 5000:5000 -v "$(pwd)":/app -d --name rest-server python-docker 

docker rm $(docker ps -a -q)

# Po spustení docker compose je možné dostať sa do containera cez príkaz
docker-compose exec app env TERM=xterm sh -c "exec bash -l"
``` 

## Ďalšie témy na preštudovanie
- volumes
```
version: "3.9"

services:
  proxy:
    build: ./proxy
    networks:
      - frontend
  app:
    build: ./app
    networks:
      - frontend
      - backend
  db:
    image: postgres
    networks:
      - backend

networks:
  frontend:
    # Use a custom driver
    driver: custom-driver-1
  backend:
    # Use a custom driver which takes special options
    driver: custom-driver-2
    driver_opts:
    foo: "1"
    bar: "2"
```
- networks
